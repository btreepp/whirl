#pragma once

#include <whirl/engines/matrix/world/actor.hpp>

namespace whirl::matrix {

bool AmIActor();
IActor* ThisActor();

}  // namespace whirl::matrix
