#pragma once

#include <whirl/engines/matrix/world/time_model.hpp>

namespace whirl::matrix {

ITimeModelPtr GetTimeModel();

}  // namespace whirl::matrix
