#pragma once

#include <whirl/node/node.hpp>
#include <whirl/node/peer_base.hpp>

#include <whirl/rpc/server.hpp>

namespace whirl {

// Peer + Main routine

class NodeBase : public INode, public PeerBase {
 public:
  void Start() override;

 private:
 protected:
  // Override this methods

  virtual void RegisterRPCServices(const rpc::IServerPtr& /*rpc_server*/) {
  }

  virtual void MainThread() {
  }

  const rpc::IServerPtr& RpcServer() {
    return server_;
  }

 private:
  // Main fiber routine
  void Main();

  void StartRpcServer();

 private:
  rpc::IServerPtr server_;
};

}  // namespace whirl
